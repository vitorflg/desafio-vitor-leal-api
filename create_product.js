import uuid from "uuid";
import * as dynamoDbAction from "./funcs/dynamodb-action";
import { success, failure } from "./funcs/response";

export async function main(event, context) {
  const data = JSON.parse(event.body);
  const params = {
    TableName: "products",
    Item: {
      productId: uuid.v1(),
      name: data.name,
      image: data.image,
      price: data.price,
      createdAt: Date.now()
    }
  };

  try {
    await dynamoDbAction.call("put", params);
    return success(params.Item);
  } catch (e) {
    console.log(e);
    return failure({});
  }
}
