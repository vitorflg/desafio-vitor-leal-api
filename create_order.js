import uuid from "uuid";
import * as dynamoDbAction from "./funcs/dynamodb-action";
import { success, failure } from "./funcs/response";

export async function main(event, context) {
  const data = JSON.parse(event.body);
  const params = {
    TableName: "orders",
    Item: {
      userId: data.userId,
      orderId: uuid.v1(),
      name: data.name,
      productId: data.productId,
      price: data.price,
      createdAt: Date.now()
    }
  };

  try {
    await dynamoDbAction.call("put", params);
    return success(params.Item);
  } catch (e) {
    console.log(e);
    return failure({});
  }
}
