import * as dynamoDbAction from "./funcs/dynamodb-action";
import { success, failure } from "./funcs/response";

export async function main(event, context) {
  const params = {
    TableName: "orders",
  };

  try {
    const result = await dynamoDbAction.call("scan", params);
    return success(result.Items);
  } catch (e) {
    console.log(e);
    return failure({});
  }
}
