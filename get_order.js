import * as dynamoDbAction from "./funcs/dynamodb-action";
import { success, failure } from "./funcs/response";

export async function main(event, context) {
  const params = {
    TableName: "orders",
    Key: {
      orderId: event.pathParameters.id
    }
  };

  try {
    const result = await dynamoDbAction.call("get", params);
    if (result.Item) {
      return success(result.Item);
    } else {
      return failure({ , error: "Item not found." });
    }
  } catch (e) {
    return failure({});
  }
}
